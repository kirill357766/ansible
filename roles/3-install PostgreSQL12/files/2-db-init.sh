#!/bin/bash

echo "Which directory you want to make postgresql root directory? (default is /data/postgres/12/data): " ; read root_dir
echo "Which directory you want to make postgresql log directory? (not similar with root directory(!!!), default is /data/postgres/log): " ; read log_dir
################################################################################################################################################################
#check if root_dir is empty
if [ -z $root_dir ]
then
root_dir="/data/postgres/12/data"
fi
################################################################################################################################################################
#create root_dir
sudo mkdir -p $root_dir
sudo chown -R postgres:postgres $root_dir
sudo chmod -R 750 $root_dir
sudo echo "$root_dir is set as db root directory"
################################################################################################################################################################
#check if log_dir is empty
if [ -z $log_dir ]
then
log_dir="/data/postgres/log"
fi
################################################################################################################################################################
#create log_dir
sudo mkdir -p $log_dir
sudo chown -R postgres:postgres $log_dir
sudo chmod -R 750 $log_dir
sudo echo "$log_dir is set as db log directory"
################################################################################################################################################################
#.bash_profile change
sudo echo -e "[ -f /etc/profile/ ] && source /etc/profile\nPGDATA=${root_dir}\nexport PGDATA\nPG_LOG=${log_dir}\nexport PG_LOG" > "/var/lib/pgsql/.bash_profile"
################################################################################################################################################################
#initialize db
sudo su -c "PGSETUP_INITDB_OPTIONS="-D ${root_dir} -X ${log_dir}" postgresql-12-setup initdb" root
################################################################################################################################################################
#insert new PGDATA in postgresql service
sudo su -c "echo -e "[Service]\nEnvironment=PGDATA=${root_dir}" > "/etc/systemd/system/postgresql-12.service.d/override.conf"" root

sudo systemctl daemon-reload

